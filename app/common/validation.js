var q = require('q');
module.exports = {

     

  
    validatedeletePost: (req) => {

        var deferred = q.defer();
       
        req.checkBody('role', 'Please enter role').notEmpty();
        req.checkBody('user_id', 'Please enter user_id').notEmpty();
        req.checkBody('post_id', 'Please enter post_id').notEmpty();
         
        if (!req.validationErrors()) {
            deferred.resolve([]);
        } else {
            deferred.resolve(req.validationErrors());
        }
        return deferred.promise;
    },

    validateupdatePost: (req) => {

        var deferred = q.defer();
        req.checkBody('post_id', 'Please enter post_id').notEmpty();
        req.checkBody('post_title', 'Please enter post_title').notEmpty();
        req.checkBody('post_content', 'Please enter post_content').notEmpty();
       
        if (!req.validationErrors()) {
            deferred.resolve([]);
        } else {
            deferred.resolve(req.validationErrors());
        }
        return deferred.promise;
    },

    validatepostbyFilter: (req) => {
        var deferred = q.defer();

        req.checkQuery('search', 'Please enter search').notEmpty();
        req.checkQuery('value', 'Please enter value').notEmpty();
        if (!req.validationErrors()) {
            deferred.resolve([]);
        } else {
            deferred.resolve(req.validationErrors());
        }

        return deferred.promise;
    },

    
    
     
    

    


    validateuserLogin: (req) => {
        var deferred = q.defer();

        req.checkBody('email', 'Please enter email').notEmpty();
        req.checkBody('password', 'Please enter password').notEmpty();
        if (!req.validationErrors()) {
            deferred.resolve([]);
        } else {
            deferred.resolve(req.validationErrors());
        }

        return deferred.promise;
    },

    validatesaveUser: (req) => {
        var deferred = q.defer();
        req.checkBody('first_name', 'Please enter first_name').notEmpty();
        
        req.checkBody('email', 'Please enter email').notEmpty();
        req.checkBody('password', 'Please enter password').notEmpty();
        req.checkBody('mobile', 'Please enter mobile').notEmpty();

        if (!req.validationErrors()) {
            deferred.resolve([]);
        } else {
            deferred.resolve(req.validationErrors());
        }

        return deferred.promise;
    },
  

 
   

     


    validateAdminLogin: (req) => {
        var deferred = q.defer();
        req.checkBody('email', 'Please enter email').notEmpty();
        req.checkBody('password', 'Please enter  password').notEmpty();
        if (!req.validationErrors()) {
            deferred.resolve([]);
        } else {
            deferred.resolve(req.validationErrors());
        }
        return deferred.promise;
    },

    validategetPostById: (req) => {
        var deferred = q.defer();
        req.checkQuery('user_id', 'Please enter user_id').notEmpty();
        
        if (!req.validationErrors()) {
            deferred.resolve([]);
        } else {
            deferred.resolve(req.validationErrors());
        }
        return deferred.promise;
    },

    

    

     
}
