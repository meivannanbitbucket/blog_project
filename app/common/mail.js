const nodemailer = require("nodemailer");
 
var q= require('q');
module.exports = {

  sendMail: async (userdata)=>{
         // async..await is not allowed in global scope, must use a wrapper
 
         let testAccount = await nodemailer.createTestAccount();

         let transporter = nodemailer.createTransport({
          service: "gmail",
         
          secure: false, // true for 465, false for other ports
          auth: {
            user: 'xxxx@gmail.com', // generated ethereal user
            pass: 'mailpassword', // generated ethereal password
          },
          tls: {
                rejectUnauthorized: false
            }
        });
        
   

  // send mail with defined transport object
  let info = await transporter.sendMail({
    from: userdata.from, // sender address
    to: userdata.to, // list of receivers
    subject: userdata.subject, // Subject line
    text: userdata.subject, // plain text body
    html: userdata.html, // html body
  });

  console.log("Message sent: %s", info.messageId);
  // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

   
  console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
//this.sendMail().catch(console.error);
      
    }
}