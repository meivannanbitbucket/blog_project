var sql = require('../../config/database.config');
var tableConfig = require('../config/table_config');
var sendmail = require('../common/mail');
var q = require('q');
var moment = require('moment');
const bcrypt = require('bcrypt');
var jwt = require('jsonwebtoken');
const saltRounds = 10;
const path = require('path');
var multer = require('multer');
const next = require('locutus/php/array/next');
const salt = bcrypt.genSaltSync(saltRounds);
// var storage = multer.diskStorage({
//     destination: function (req, file, cb) {
//         cb(null,'/uploads')
//     },
//     filename: function (req, file, cb) {
//         var datetimestamp = Date.now();
//         cb(null, file.fieldname + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1])
//     }
// });
var storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, './public/uploads')
    },
    filename: (req, file, cb) => {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
    }
});
var upload = multer({
    storage: storage
}).single('post_image');
module.exports = {

    userLogin: (req) => {
        var deferred = q.defer();
        let {email,password}=req.body
         
        
        var checking_query = "select email,id from users where email='"+email+"'";
        sql.query(checking_query, function (err, data) {

            if (err) {
                console.log(err);
                deferred.resolve({ status: 0, message: "Error occur to reteive data" });
            } else {
                console.log('data',data)
                if (data.length > 0) {
                    
                    var query="select email,password,name from users where role_id=2 and id='"+data[0].id+"'";
                    sql.query(query,async function(err,response){
                        
                        if (err) {
                            console.log(err);
                            deferred.resolve({ status: 0, message: "Error occur to reteive data" });
                        }
                        if(response.length > 0)
                        {
                            const hash = bcrypt.hashSync('admin123', salt);
                            console.log('password',password,response,response[0].password,hash)
                            let check_password= await bcrypt.compare(password, response[0].password);
                            if(check_password==true)
                            {
                                var token = jwt.sign({ id: response[0].id }, 'supersecret', {
                                    expiresIn: 86400
                                });
                                deferred.resolve({ status: 1, message: "User LoggedIn Successfully",data:{
                                    id:response.length>0?response[0].id:0,
                                    email:response.length>0?response[0].email:'',
                                    username:response.length>0?response[0].name:'',
                                    token:token
                                } });
                            }
                            else
                            {
                                deferred.resolve({ status: 0, message: "Password is incorrect" });
                            }

                        }
                        else
                        {
                            deferred.resolve({ status: 0, message: "incorrect username" });
                        }

                    })

                } else {
                    deferred.resolve({ status: 0, message: "User is not found" });
                }
            }
        });
        return deferred.promise;
    },

    saveUser: (req) => {
        var deferred = q.defer();

         let {first_name,email,password,mobile}=req.body
        var role_id=2
        var hashedpassword=bcrypt.hashSync(password, salt);
        var checkingQuery="select * from users where email='"+email+"' and role_id='"+role_id+"' and status=0"
        sql.query(checkingQuery,function(err,response)
        {
if(err)
{
    console.log(err)
    deferred.resolve({ status: 0, message: "Failed to check user" });
}
else{
    if(response.length==0)
{
    var saveuserQuery = "INSERT INTO " + tableConfig.USER + " (name,email,password,mobile,role_id) VALUES ('" + first_name + "','" + email + "','" + hashedpassword + "', '" + mobile + "','" + role_id + "')";
        sql.query(saveuserQuery, function (err, data) {
            if (err) {
                console.log(err);
                deferred.resolve({ status: 0, message: "Failed to save user" });
            } else {
                if (data.affectedRows > 0) {
                    deferred.resolve({ status: 1, message: "User registration completed Successfully" });
                } else {
                    deferred.resolve({ status: 0, message: "Failed to save user" });
                }
            }
        });
}
else
{
    deferred.resolve({ status: 0, message: "Email is already exist" });
}
}
        })
        
        return deferred.promise;
    },

    savePost: (req) => {
        var deferred = q.defer();
        upload(req, next, async function (err) {
            
            let {user_id,title,descripation,post_image}=req.body
            var emaillistQuery="select * from users as u join posts as p on p.author_id=u.id where   u.role_id=2 and u.status=0 order by u.id"
             var emaillist=[]
         var role_id=2
         
        var checkingQuery="select * from users where id='"+user_id+"' and role_id='"+role_id+"' and status=0"
        sql.query(checkingQuery,function(err,response)
        {
if(err)
{
    console.log(err)
    deferred.resolve({ status: 0, message: "Failed to check user" });
}
else{
    if(response.length>0)
{
    let imagepath=req.file!=undefined?'public/uploads/'+ req.file.filename:''
    var savepostQuery = "INSERT INTO " + tableConfig.POSTS + " (author_id,title,descripation,post_image) VALUES ('" + user_id + "','" + title + "','" + descripation + "','"+ imagepath + "')";
        sql.query(savepostQuery, function (err, data) {
            console.log(savepostQuery)
            if (err) {
                console.log(err);
                deferred.resolve({ status: 0, message: "Failed to save post" });
            } else {
                 
                if (data.affectedRows > 0) {

                    //email query
                    sql.query(emaillistQuery,function (err,emaildata) {
                        if (err) {
                            console.log(err)
                            deferred.resolve({ status: 0, message: "Failed to send mail" });
                        } else {
                            if (emaildata.length > 0) {
                                console.log('emaildata',emaildata)
                                emaildata.forEach(element => {
                                   emaillist.push(element.email)
                                });
                                console.log('emaillist',emaillist)
                                let template={}


                                template.from='meivannanjayalakshim@gmail.com'
                                template.to=emaillist.join(',')
                                template.subject=title+'has been published'
                                template.text=`Hi` +response[0].name+`
                                It’s really annoying, right? You’ve been working hard on [task] for hours, then you start struggling to make progress, and now you’re stuck.Believe us, you’re not alone. In fact, these are very common problems that many of our readers have been facing.But don’t worry, because we’re here to help. We’ve just published a new guide to help people just like you, which you can read on our website by clicking the button below.
                                
                                `+title+`has been published by `+response[0].name
                                template.html=`<b>Hello</b>` +response[0].name+`<p> It’s really annoying, right? You’ve been working hard on [task] for hours, then you start struggling to make progress, and now you’re stuck.Believe us, you’re not alone. In fact, these are very common problems that many of our readers have been facing.But don’t worry, because we’re here to help. We’ve just published a new guide to help people just like you, which you can read on our website by clicking the button below.
                                
                                `+title+`has been published by `+response[0].name+`</p>`
                                let email=sendmail.sendMail(template)
                                
                            deferred.resolve({ status: 1, message: "Post published Successfully" });
                            } else {
                                
                            }
                            
                        }
                       
                    })
                    

                   
                } else {
                    deferred.resolve({ status: 0, message: "Failed to save post" });
                }
            }
        });
}
else
{
    deferred.resolve({ status: 0, message: "User is not found" });
}
}
        })
        })
         
        
        return deferred.promise;
    },

    adminLogin: (req) => {
        var deferred = q.defer();

         
        let {email,password}=req.body
        var adminQuery = "select * from " + tableConfig.USER + " where email='" + email + "' and role_id=1 and status=0";

        sql.query(adminQuery, async function (err, admindata) {
            if (err) {
                console.log(err);
                deferred.resolve({ status: 0, message: "Something Went Wrong" });
            } else {
                if (admindata.length > 0) {
                    let check_password= await bcrypt.compare(password, admindata[0].password);
                    if(check_password==true)
                    {
                        deferred.resolve({ status: 1, message: "Admin loggedIn successfully" });
                    }
                    else
                    {
                        deferred.resolve({ status: 0, message: "Incorret password" });
                    }

                     
                    
                } else {
                    deferred.resolve({ status: 0, message: "email is not found" });
                }
            }
        });

        return deferred.promise;
    },

    getallposts: (req) => {
        var deferred = q.defer();
         
        var PostQuery = " select title,author_id from " + tableConfig.POSTS + " where status=0";
        sql.query(PostQuery, function (err, posttdata) {
            if (err) {
                console.log(err);
                deferred.resolve({ status: 0, message: "Something Went Wrong" });
            } else {
                if (posttdata.length > 0) {
                    
                   
                    deferred.resolve({ status: 1, message: "Post list Successfully", posttdata: posttdata });
                } else {
                    deferred.resolve({ status: 0, message: "No data found", posttdata: [] });
                }
            }
        });
        return deferred.promise;
    },


   getpostsById: (req) => {
        var deferred = q.defer();
         let {user_id}=req.query
        var PostQuery = " select title,author_id,descripation from " + tableConfig.POSTS + " where author_id='"+user_id+"' and status=0";
        sql.query(PostQuery, function (err, posttdata) {
            if (err) {
                console.log(err);
                deferred.resolve({ status: 0, message: "Something Went Wrong" });
            } else {
                if (posttdata.length > 0) {
                    
                   var UserQuery="select CONCAT(name) as user_name from " + tableConfig.USER + " Where id='"+posttdata[0].author_id+"'"
                   sql.query(UserQuery,function(err,userdata)
                   {
                       
                       if(err)
                       {
                           deferred.resolve({ status: 0, message: "Something wentwrong", error: err })
                       }
                       else
                       {
                            
                           posttdata[0].user_name=userdata.length>0?userdata[0].user_name:'' 
                        deferred.resolve({ status: 1, message: "Post list Successfully", posttdata: posttdata });
                       }
                   })
                   
                } else {
                    deferred.resolve({ status: 0, message: "No data found", posttdata: [] });
                }
            }
        });
        return deferred.promise;
    },


    deletepostsById: (req) => {
        var deferred = q.defer();
         let {post_id,role,user_id}=req.body
         var condition=''
         if(role==1) //admin
         {
             condition="where id='"+post_id+"'"
         }
         if(role==2)
         {
             condition="where id='"+post_id+"' and author_id='"+user_id+"'"
         }
        var PostQuery = "update  " + tableConfig.POSTS + "  set status=1 "+condition+"";
        sql.query(PostQuery, function (err, posttdata) {
            if (err) {
                console.log(err);
                deferred.resolve({ status: 0, message: "Something Went Wrong" });
            } else {
                if (posttdata.affectedRows > 0) {
                    
                    deferred.resolve({ status: 1, message: "Deleteion done Successfully" });
                   
                } else {
                    deferred.resolve({ status: 0, message: "No data found", posttdata: [] });
                }
            }
        });
        return deferred.promise;
    },

 

    updatepostsById: (req) => {
        var deferred = q.defer();
         let {post_title,post_content,post_id}=req.body
         var condition=''
         
          
             condition="where id='"+post_id+"' and status=0"
         
        var PostQuery = "update  " + tableConfig.POSTS + " set  title='"+post_title+"',descripation='"+post_content+"' "+condition+" ";
        sql.query(PostQuery, function (err, posttdata) {
            if (err) {
                console.log(err);
                deferred.resolve({ status: 0, message: "Something Went Wrong" });
            } else {
                if (posttdata.affectedRows > 0) {
                    
                    deferred.resolve({ status: 1, message: "Update Post Successfully"});
                    
                   
                } else {
                    deferred.resolve({ status: 0, message: "No data found", posttdata: [] });
                }
            }
        });
        return deferred.promise;
    },

    

 
    
    


    

         
      
    
}




 