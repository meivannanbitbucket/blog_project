var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());
var validation = require('../common/validation');
var blogeModels = require('../models/blogModels');

module.exports = {

    userLogin: (req, res) => {
        validation.validateuserLogin(req).then((validationResults) => {

            if (validationResults.length == 0) {
                blogeModels.userLogin(req).then(results => {
                    res.json(results);
                });

            } else {
                res.json({ status: 0, message: validationResults[0].msg });
            }
        });

    },
 
  

    saveUser: (req, res) => {

        validation.validatesaveUser(req).then((validationResults) => {
            if (validationResults.length == 0) {
                

                blogeModels.saveUser(req).then(results => {
                    res.json(results);
                });

            } else {
                res.json({ status: 0, message: validationResults[0].msg });
            }
        });
    },
    savePost: (req, res) => {

        

                blogeModels.savePost(req).then(results => {
                    res.json(results);
                });

             
         
    },

    adminLogin: (req, res) => {
        validation.validateAdminLogin(req).then((validationResults) => {
            if (validationResults.length == 0) {
                
                blogeModels.adminLogin(req).then(results => {
                    res.json(results);
                });

            } else {
                res.json({ status: 0, message: validationResults[0].msg });
            }
        });
    },

    getAllPosts: (req, res) => {
        

                blogeModels.getallposts(req).then(results => {
                    res.json(results);
                });

          
    },

    getpostById: (req, res) => {
       
        validation.validategetPostById(req).then((validationResults) => {
            if (validationResults.length == 0) {
               

            blogeModels.getpostsById(req).then(results => {
                res.json(results);
            });
        } else {
            res.json({ status: 0, message: validationResults[0].msg });
        }
    });
        
       
           
    },
   
    updatePost: (req, res) => {
        validation.validateupdatePost(req).then((validationResults) => {

            if (validationResults.length == 0) {
              
                blogeModels.updatepostsById(req).then(results => {
                    res.json(results);
                });

            } else {
                res.json({ status: 0, message: validationResults[0].msg });
            }
        });
    },

    deletePosts: (req, res) => {
        
        validation.validatedeletePost(req).then((validationResults) => {
            if (validationResults.length == 0) {
                 

                blogeModels.deletepostsById(req).then(results => {
                    res.json(results);
                });
            } else {
                res.json({ status: 0, message: validationResults[0].msg });
            }
        });
    },
    
     
    

    

}