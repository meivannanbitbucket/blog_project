
module.exports = (app) => {

     
    const blogController=require('../controllers/blogController')
     
     
    app.post('/blog/userLogin', blogController.userLogin);
    app.post('/blog/add_post', blogController.savePost);  
    app.post('/blog/user_register', blogController.saveUser);
    app.post('/blog/admin_login', blogController.adminLogin);
    app.put('/blog/update_post', blogController.updatePost)
    app.delete('/blog/delete_post', blogController.deletePosts)
     
    app.get('/blog/get_user_id',  blogController.getpostById);
    app.get('/blog/get_all_posts', blogController.getAllPosts);
};