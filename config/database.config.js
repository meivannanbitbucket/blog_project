'user strict';

var mysql = require('mysql');
var HOSTNAME = 'localhost';
var USER = 'root';
var PASSWORD = '';
var DB_NAME = 'redink_blog';
var TIMEZONE = 'utc'
var PORT = 3306;

//mysql db connection
var pool = mysql.createPool({
  host: HOSTNAME,
  user: USER,
  password: PASSWORD,
  database: DB_NAME,
  timezone: TIMEZONE
});
pool.getConnection(function (err, connection) {
  if (err) throw err; // not connected!

  // Use the connection
  connection.query('SELECT 1', function (error, results, fields) {
    // When done with the connection, release it.
    if (!error) {
      console.log("DB connected successfully")
      connection.release();
    }


    // Handle error after the release.
    if (error) throw error;


    // Don't use the connection here, it has been returned to the pool.
  });
})
module.exports = pool;
